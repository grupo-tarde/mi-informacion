# ***REPORTES***
## FUNCIONES
## GUEST

* No se puede hacer push al repositorio
* Se puede crear ramas localmente pero no en el repositorio
* No se puede hacer pull
* Se puede clonar un repositorio
* Se puede inicializar un repositorio pero no subirlo a gitlab
* Se puede realizar commit localmente pero no subirlo al gitlab
* No se puede hacer merge entre ramas
* No se puede añadir miembros al repositorio
* No se puede asignar o modificar permisos a los usuarios del repositorio
* Si puede crear issues
* No se puede asignar Issues a miembros
* No se puede asignar milestones
* No se puede asignar labels
* No puede asignar merge request

**No se puede modificar, eliminar ni crear archivos o carpetas con la funcion Guest, unicamente desde un entorno local, pero no se puede subir al gitlab**

## REPORTER

**No se puede modificar, eliminar ni crear archivos o carpetas con la funcion Guest, unicamente desde un entorno local, pero no se puede subir al gitlab**
* Con la funcion Reporter se puede hacer pull
* No se puede hacer push, merge, ni crear ramas en el repositorio
* Se puede clonar el repositorio
* No se puede añadir miembros al repositorio
* No se puede asignar o modificar permisos a los usuarios del repositorio
* Si puede crear y asignar issues a usuarios
* Si puede asignar labels y milstones
* No puede asignar merge request

## DEVELOPER

* Se puede clonar el repositorio
* Se puede modificar, eliminar y crear archivos y carpetas unicamente desde otras ramas
* Con la funcion developer no se puede crear, eliminar y modificar archivos desde la rama master
* Con la funcion developer se puede crear varias ramas
* Se puede hacer merge entre ramas
* Se puede hacer pull
* No se puede añadir miembros al repositorio
* No se puede asignar o modificar permisos a los usuarios del repositorio
* Puede crear y asignar issues a miembros
* Puede asignar labels y milestones
* Puede asignar merge request

**No se puede crear, ni modificar o eliminar informacion en la rama master con el usuario developer**

## MAINTAINER

* Con el usuario Maintainer se puede crea, modificar y eliminar archivos y carpetas.
* Se puede hacer pull, push
* Se puede crear ramas y hacer merge entre estas y hacia la rama master
* Podemos añadir miembros al repositorio
* Se puede asignar issues , crearlos
* Se puede asignar labels
* Puede asignar merge request